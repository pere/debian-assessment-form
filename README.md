A useful form for record keeping and feedback collection
========================================================

Inspired by the Arch Linux assessment form, I concluded that the
Debian project would be better off with a similar form, slightly
adjusted to better work with the target audience.

Fetch latest edition from https://salsa.debian.org/pere/debian-assessment-form .

The LaTeX code is derived from the LaTeX form by Håvard Pettersson
downloaded from
https://gist.githubusercontent.com/haavard/56b8d522893e3c4bb9645c48444a5f5c/raw/7050a9db8ab7465110b446c538957bba69a77c9b/alfa.latex
and the Debian swirl downloaded from
https://www.debian.org/logos/openlogo-nd.eps .
