all: debian-form.pdf

debian-form.pdf: openlogo-nd.eps debian-form.tex
	pdflatex debian-form

clean:
	$(RM) *~ openlogo-nd-eps-converted-to.pdf *.aux *.log *.out

distclean: clean
	$(RM) debian-form.pdf
